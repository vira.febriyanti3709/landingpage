<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>SLICING WEB</title>
    <link rel="stylesheet" href="<?= base_url() ?>assets/css/bootstrap.css">
    <link rel="stylesheet" href="<?= base_url() ?>assets/css/bootstrap-grid.css">
    <!-- <link rel="stylesheet" href="<?= base_url() ?>assets/libs/fontawesome/css/fontawesome.css">
    <link rel="stylesheet" href="<?= base_url() ?>assets/libs/fontawesome/css/fontawesome.min.css">
    <link rel="stylesheet" href="<?= base_url() ?>assets/libs/fontawesome/css/all.css"> -->
    <link rel="stylesheet" href="<?= base_url() ?>assets/libs/cdn/bootstrap.min.css">
    <link rel="stylesheet" href="<?= base_url() ?>assets/style.css">
</head>

<body>

    <!-- navbar -->

    <nav class="navbar navbar-expand-lg bg-light">
        <div class="container-fluid background-putih">
            <a class="navbar-brand" href="#">
                <img src="<?= base_url() ?>assets/Image/logo.jpeg" width="100" alt="">
            </a>
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav ms-auto">
                    <li class="nav-item">
                        <a class="nav-link active" aria-current="page" href="#">BERANDA</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">ABOUT</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">PORTOFOLIO</a>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                            SERVICES
                        </a>
                        <ul class="dropdown-menu">
                            <li><a class="dropdown-item" href="#">WEBSITE INSTAN</a></li>
                            <li>
                                <hr class="dropdown-divider">
                            </li>
                            <li><a class="dropdown-item" href="#">COMPANY PROFILE</a></li>
                            <li>
                                <hr class="dropdown-divider">
                            </li>
                            <li><a class="dropdown-item" href="#">ONLINE SHOP</a></li>
                        </ul>
                    </li>
                    <li class='loginContainer'>
                        <a href="" class='login-btn'>Masuk</a>
                        <a href="" class='login-btn'>Daftar</a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
    <!-- navbar end -->
    <section class="jumbotron p-5 mainView">
        <!-- <div class=""> -->
        <div class="container-fluid" data-aos="fade-up">
            <div class="row justify-content-center">
                <div class="col-lg-6 mainLeft">
                    <h2 class="text-center">Fokuslah pada Bisnis Anda, Biarkan Kami Membuat Website Anda!</h2><br>
                    <p>Jadikan bisnis Anda lebih menguntungkan dengan jasa membuat website terpercaya yang didukung hosting premium dari VFwebhost. Saat ini, lebih dari 500 perusahaan telah mempercayakan pembuatan websitenya pada VFwebhost.</p><br><br>
                    <a href="#" class="btn background-red text-white">Lihat Penawaran</a>
                </div>
                <div class="col-lg-6 mainRight" data-aos="zoom-in" data-aos-delay="150">
                    <img src="<?= base_url() ?>assets/Image/web.jpg" class="img-fluid mb-n4">
                </div>
            </div>
        </div>
    </section>

    <!-- harga -->
    <!-- HARGA -->
    <section class="py-4">
        <div class="container text-center">
            <div class="d-flex justify-content-center">
                <h2 class='fw-bold'>Harga Pembuatan Website </h2>
            </div>
            <div class="row">
                <div class="col-md-4 mb-5">
                    <div class="media border border-danger align-items-center py-2 px-2 rounded">
                        <div class="media-body ml-1">
                            <h4>WEBSITE INSTAN</h4>
                            <button type="button" class="btn background-abu " style="--bs-btn-padding-y: .10rem; --bs-btn-padding-x: .5rem; --bs-btn-font-size: .80rem;">
                                Paket 3 Tahun
                            </button>
                            <h6 class="text-secondary"><del>Rp 650.000 /tahun</del></h6>
                            <h5><del>Rp 550.000 /tahun</del></h5>
                            <div class="d-grid gap-2 d-md-block">
                                <button class="btn btn background-red" type="button">Pesan Website</button>
                                <button type="button" class="btn btn-outline-danger text-black">Detail</button>
                                <p>Cocok bagi Anda yang ingin mempunyai website
                                    untuk kebutuhan bisnis, dengan biaya yang
                                    terjangkau dan dapat dikelola secara mandiri.
                                    Instalasi hanya butuh waktu 1x24 jam.
                                </p><br>
                                <li>Gratis Hosting dan Domain</li>
                                <li>Berbagai Macam Pilihan Tema</li>
                                <li>SEO Friendly</li>
                                <li>Desain Responsif</li>
                                <li>Instalasi Cepat</li>
                                <li>Terdapat Banyak Tutorial</li>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4  mb-5">
                    <div class="media border border-danger align-items-center py-2 px-2 rounded">
                        <div class="media-body ml-1">
                            <h4>COMPANY PROFILE</h4>
                            <button type="button" class="btn background-abu" style="--bs-btn-padding-y: .10rem; --bs-btn-padding-x: .5rem; --bs-btn-font-size: .80rem;">
                                Paket 3 Tahun
                            </button>
                            <h6 class="text-secondary"><del>Rp 1700.000 /tahun</del></h6>
                            <h5><del>Rp 1.500.000 /tahun</del></h5>
                            <div class="d-grid gap-2 d-md-block">
                                <button class="btn btn background-pink" type="button">Pesan Website</button>
                                <button type="button" class="btn btn-outline-danger text-black">Detail</button>
                                <p>Cocok bagi Anda yang ingin mempunyai website
                                    untuk kebutuhan bisnis, dengan biaya yang
                                    terjangkau dan dapat dikelola secara mandiri.
                                    Instalasi hanya butuh waktu 1x24 jam.
                                </p><br>
                                <li>Free Unlimited Hosting</li>
                                <li>Free Maintenance Setiap Bulan</li>
                                <li>Gratis Setup 5 Konten/Artikel</li>
                                <li>1 Halaman Awal</li>
                                <li>1 Halaman Awal</li>
                                <li>500+ Tema Pilihan</li>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4  mb-5">
                    <div class="media border border-danger align-items-center py-2 px-2 rounded">
                        <div class="media-body ml-1">
                            <h4>ONLINE SHOP</h4>
                            <button type="button" class="btn background-abu " style="--bs-btn-padding-y: .10rem; --bs-btn-padding-x: .5rem; --bs-btn-font-size: .80rem;">
                                Paket 3 Tahun
                            </button>
                            <h6 class="text-secondary"><del>Rp 3000.000 /tahun</del></h6>
                            <h5><del>Rp 2500.000 /tahun</del></h5>
                            <div class="d-grid gap-2 d-md-block">
                                <button class="btn btn background-red" type="button">Pesan Website</button>
                                <button type="button" class="btn btn-outline-danger text-black">Detail</button>
                                <p>Cocok bagi Anda yang ingin mempunyai website
                                    untuk kebutuhan bisnis, dengan biaya yang
                                    terjangkau dan dapat dikelola secara mandiri.
                                    Instalasi hanya butuh waktu 1x24 jam.
                                </p><br>
                                <li>Gratis Setup 5 Produk Unggulan</li>
                                <li>Integrasi Marketplace Berbagai Pilihan Tema</li>
                                <li>Integrasi Marketplace / Instagram Shop</li>
                                <li> 5 Halaman Awal</li>
                                <li>1x Revisi Desain</li>
                                <li>500+ Tema Pilihan</li>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </section>

    <!-- Alasan -->

    <section id="why">
        <div class="container-fluid why-bg">
            <div class="row">
                <div class="col text-center">
                    <h2>Mengapa Harus Membuat Website di VFWebHost?</h2>
                </div>
            </div>
            <div class="row mt-5 whyItems">
                <div class="col-lg-4 justify-content-center">
                    <div class="card-why">
                        <img src="<?= base_url() ?>assets/Image/icon/phone.svg" alt="time"/>
                        <h3>Mobile Friendly</h3>
                        <p>Website yang kami buat sudah
                            mobile friendly dan responsive.
                            Sehingga tampilan website Anda
                            akan menyesuaikan layar dari
                            perangkat yang digunakan.</p>
                    </div>
                </div>
                <div class="col-lg-4 justify-content-center">
                    <div class="card-why">
                        <div class="">
                            <img src="<?= base_url() ?>assets/Image/icon/seo.svg" alt="time"/>
                        </div>
                        <h3>SEO Friendly</h3>
                        <p>Semua website yang kami buat sudah mengikuti kaidah
                            SEO (Search Engine Optimization),
                            sehingga bisnis Anda dapat lebih
                            mudah ditemukan di mesin pencari.</p>
                    </div>
                </div>
                <div class="col-lg-4 justify-content-center">
                    <div class="card-why">
                        <div class="">
                            <img src="<?= base_url() ?>assets/Image/icon/template.svg" alt="time"/>
                        </div>
                        <h3>Template Siap Pakai</h3>
                        <p>Pilih sendiri tema dan desain yang
                            sesuai dengan kebutuhan
                            website Anda. Ada ratusan
                            tema menarik yang bisa Anda pilih</p>
                    </div>
                </div>
            </div>
            <div class="row mt-5">
                    <div class="col-lg-4 justify-content-center">
                        <div class="card-why">
                                <img src="<?= base_url() ?>assets/Image/icon/domain.svg"
                                alt="time" class="" />
                            <!-- <div class="">
                            </div> -->
                            <h3>Gratis Domain &
                                Hosting</h3>
                            <p>Anda tidak perlu memikirkan biaya
                                untuk domain dan hosting.
                                Semua paket jasa website terbaik
                                kami sudah termasuk hosting dan domain
                                gratis untuk tahun pertama.</p>
                        </div>
                    </div>
                    <div class="col-lg-4 justify-content-center">
                        <div class="card-why">
                            <div class="">
                                <img src="<?= base_url() ?>assets/Image/icon/layanan.svg" alt="time" />
                            </div>
                            <h3>Layanan Prima 24/7</h3>
                            <p>VFwebhost siap membuat website
                                yang menarik, ringan diakses, kredibel,
                                dan juga memberikan dukungan support
                                troubleshooting. Anda hanya perlu fokus
                                pada produk dan promosi saja.</p>
                        </div>
                    </div>
                    <div class="col-lg-4 justify-content-center">
                        <div class="card-why">
                            <div class="">
                                <img src="<?= base_url() ?>assets/Image/icon/server.svg" alt="time" />
                            </div>
                            <h3>Server Terbaik</h3>
                            <p>Didukung dengan server yang
                                kuat & aman, sumber daya
                                besar dan berbagai fitur
                                optimasi: auto-backup,
                                auto-patch, & auto-installer.</p>
                        </div>
                    </div>
            </div>
        </div>
        </div>
        <!-- BERANDA -->



        <!-- BERANDA END -->


        <!-- BERANDA END-->

        <!-- PORTOFOLIO -->
        <section id="portofolio">
            <div class="container portofolio-bg">
                <div class="row">
                    <div class="col mb-5 text-center">
                        <h2>PORTOFOLIO WEBSITE YANG PERNAH KAMI BUAT</h2>
                    </div>
                </div>
                <div id="carouselExampleIndicators" class="carousel slide" data-bs-ride="true">
                    <div class="carousel-indicators">
                        <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="0" class="active" aria-current="true" aria-label="Slide 1"></button>
                        <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="1" aria-label="Slide 2"></button>
                        <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="2" aria-label="Slide 3"></button>
                    </div>
                    <div class="carousel-inner">
                        <div class="carousel-item active">
                            <img src="<?= base_url() ?>assets/Image/carousel.png" class="d-block w-100" alt="...">
                        </div>
                        <div class="carousel-item">
                            <img src="<?= base_url() ?>assets/Image/carousel.png" class="d-block w-100" alt="...">
                        </div>
                        <div class="carousel-item">
                            <img src="<?= base_url() ?>assets/Image/carousel.png" class="d-block w-100" alt="...">
                        </div>
                    </div>
                    <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide="prev">
                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span class="visually-hidden">Previous</span>
                    </button>
                    <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide="next">
                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                        <span class="visually-hidden">Next</span>
                    </button>
                </div>
            </div>
        </section>
        <!-- PORTOFOLIO END -->
        <section id="testimoni">
            <div class="container testimoni-bg">
                <div class="row">
                    <div class="col mb-5 text-center">
                        <h2 class="test1">Testimoni</h2>
                        <p class="test2">Berikut Beberapa Ulasan Mengenai Jasa Kami</p>
                        <img src="<?= base_url() ?>assets/Image/komen1.jpeg" alt="">
                        <img src="<?= base_url() ?>assets/Image/komen2.jpeg" alt="">
                    </div>
                </div>
            </div>
        </section>
        <!-- HARGA END -->
        <section class="jumbotron p-5 closing">
            <!-- <div class=""> -->
            <div class="container-fluid">
                <div class=" row justify-content-center">
                    <div class="col-xl-9">
                        <h2 class="text-center">Kesulitan mengelola bisnis secara manual?</h2>
                        <p>VFwebhost hadir menjawab keresahan anda,
                            Di zaman yang sudah serba online ini, VFwehost siap membantu untuk mewujudkan dan mengelola website anda agar lebih profesional dan mudah digunakan.
                            Raih kemudahan memiliki website impian anda sekarang !</p><br><br>
                        <a href="#" class="btn background-red text-white">Lihat Penawaran</a>
                    </div>
                </div>
            </div>
        </section>
        
        <footer class="background-red">
                <!-- <div class="d-flex"> -->
                    <!-- <div class="col-lg-3 col-md-6"> -->
                        <strong> Alamat :</strong> University of Riau, Kampus Bina Widya
                        Jl. HR. Soebrantas KM. 12,5 Simpang Baru
                        Tampan, Pekanbaru, Indonesia<br>
                        <strong>Phone :</strong> 0822-8478-xxxx<br>
                        <strong>Email :</strong> vfwebhost@gmail.com<br>
                        <strong>Instagram :</strong> @vfwebhost<br>
                    <!-- </div> -->
                <!-- </div> -->
        </footer>





        <script src="<?= base_url() ?>assets/js/jquery.js"></script>
        <script src="<?= base_url() ?>assets/js/bootstrap.js"></script>
        <!-- <script src="<?= base_url() ?>assets/libs/fontawesome/js/fontawesome.js"></script>
        <script src="<?= base_url() ?>assets/libs/fontawesome/js/all.js"></script> -->
        <script src="<?= base_url() ?>assets/libs/cdn/bootstrap.bundle.min.js"></script>
</body>

</html>